import TinctManager from "./manager";
import * as global from 'global';

if (!global.tinct) {
	global.tinct = new TinctManager();
}

export default global.tinct;