import manager from './instantiator';
import Color from './color';
import Shader from '../shaders/class';
import Swatch from '../swatches/class';

const Tinct = manager;

export default Tinct;
export {
	Color,
	Shader,
	Swatch,
};