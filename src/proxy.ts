import TinctManager from "./manager";

const proxyDefinition = {
	has: (target: TinctManager, prop: string): boolean => {
		if (prop in target) {
			return true
		} else {
			try {
				target.resolveColor(prop);
				return true;
			} catch (e) {
				return false;
			}
		}
	},
	get: (target: TinctManager, prop: string): any => {
		if (prop in target) {
			return target[prop];
		} else {
			return target.resolveColor(prop);
		}
	}
};

export default proxyDefinition;
