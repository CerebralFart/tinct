enum NameStyle {
	PREFIX = 'prefix',
	SUFFIX = 'suffix'
}

export default NameStyle;