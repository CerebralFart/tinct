import Color from "./color";
import {range256} from './d';

export const chunkString = (string: string, length: number): string[] => {
	if (length === 1)
		return string.split("");
	else
		return string.match(new RegExp('.{1,' + length + '}', 'g'));
};

export const parseToComponent = (c: string): number => {
	if (!c.match(/^[0-9a-fA-F]+$/))
		return NaN;

	let n = parseInt(c, 16);

	if (c.length === 1)
	//This maps all single character values between 255 and 0.
	//n * 17 = n * 16 + n. In effect this duplicates the character and then converts it back from base 16.
		return n * 17;
	else
		return n;
};

export const parseColor = (color: string): Color => {
	if (color.charAt(0) === '#')
		color = color.substring(1);

	let l;
	if (color.length === 3 || color.length === 4) l = 1;
	else if (color.length === 6 || color.length === 8) l = 2;
	else throw new Error(`Could not parse string ${color} to color`);

	let components = chunkString(color, l).map(parseToComponent);

	if (components.length < 3 || components.length > 4) {
		throw new Error(`Internal error in decomposing color`);
	} else if (components.filter(isNaN).length > 0) {
		throw new Error(`Decomposing color resulted in NaN, likely contains non-hex characters`)
	} else {
		let [r, g, b, a] = <range256[]>components;
		return new Color([r, g, b, a]);
	}

};

export const mixColors = (color1: Color, color2: Color, amount: number): Color => {
	let {red: r1, green: g1, blue: b1} = color1;
	let {red: r2, green: g2, blue: b2} = color2;

	let component1 = color1.getAlphaFraction() * (1 - amount);
	let component2 = color2.getAlphaFraction() * amount;

	let r: range256 = <range256>(r1 * component1 + r2 * component2);
	let g: range256 = <range256>(g1 * component1 + g2 * component2);
	let b: range256 = <range256>(b1 * component1 + b2 * component2);

	return new Color([r, g, b]);
};