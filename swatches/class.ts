import {ColorProvider} from "../src/d";
import Color from "../src/color";

class Swatch implements ColorProvider {
	private name = "";
	private colors: { [key: string]: Color } = {};
	private shades: { [key: string]: Color } = {};

	constructor(data: SwatchInitializer) {
		this.name = data.name;

		if (data.colors) {
			this.colors = data.colors;
		}

		if (data.shades) {
			this.shades = data.shades;
		}
	}

	public getName(): string {
		return this.name;
	}

	public hasColor(name: string): boolean {
		return name in this.colors
	}

	public getColor(name: string): Color {
		if (this.hasColor(name)) {
			return this.colors[name];
		} else {
			throw new Error(`Error ${name} could not be found`)
		}
	}

	public getColors(): string[] {
		return Object.keys(this.colors);
	}

	public hasShade(name: string): boolean {
		return name in this.shades;
	}

	public getShade(name: string): Color {
		if (this.hasShade(name)) {
			return this.shades[name];
		} else {
			throw new Error(`Shade ${name} could not be found`);
		}
	}

	public getShades(): string[] {
		return Object.keys(this.shades);
	}
}

export default Swatch;

export interface SwatchInitializer {
	name: string;
	colors?: { [key: string]: Color };
	shades?: { [key: string]: Color };
}