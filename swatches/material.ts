import Swatch from "./class";
import Color from "../src/color";

const materialSwatch = new Swatch({
	name: 'material',
	colors: {
		red: new Color("#F44336"),
		pink: new Color("#E91E63"),
		purple: new Color("#9C27B0"),
		deepPurple: new Color("#673AB7"),
		indigo: new Color("#3F51B5"),
		blue: new Color("#2196F3"),
		lightBlue: new Color("#03A9F4"),
		cyan: new Color("#00BCD4"),
		teal: new Color("#009688"),
		green: new Color("#4CAF50"),
		lightGreen: new Color("#8BC34A"),
		lime: new Color("#CDDC39"),
		yellow: new Color("#FFEB3B"),
		amber: new Color("#FFC107"),
		orange: new Color("#FF9800"),
		deepOrange: new Color("#FF5722"),
		brown: new Color("#795548"),
		blueGrey: new Color("#607D8B"),
		grey: new Color("#9E9E9E"),
	},
	shades: {
		black: new Color("#000000"),
		white: new Color("#FFFFFF")
	}
});

export default materialSwatch;