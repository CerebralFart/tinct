import Swatch from "./class";
import Color from "../src/color";

const basicSwatch = new Swatch({
	name: 'basic',
	colors: {
		red: new Color("#FF0000"),
		yellow: new Color("#FFFF00"),
		green: new Color("#00FF00"),
		cyan: new Color("#00FFFF"),
		blue: new Color("#0000FF"),
		purple: new Color("#FF00FF"),
		grey: new Color("#808080"),
	},
	shades: {
		black: new Color("#000000"),
		white: new Color("#FFFFFF"),
	}
});

export default basicSwatch;