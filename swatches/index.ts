import basicSwatch from "./basic";
import cssSwatch from "./css";
import materialSwatch from "./material";
import Swatch from "./class";

export default Swatch;

export {
	basicSwatch,
	cssSwatch,
	materialSwatch
};