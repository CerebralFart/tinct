import NameStyle from "../src/nameStyle";
import {ShaderFunction} from "../src/d";

export default class ShaderPack {
	private name = "";
	private nameStyle = NameStyle.PREFIX;
	private shaders: { [key: string]: ShaderFunction } = {};

	constructor(data) {
		this.name = data.name;
		this.shaders = data.shaders;

		if (data.nameStyle) {
			this.nameStyle = data.nameStyle;
		}
	}

	public getName(): string {
		return this.name;
	}

	public getNameStyle(): NameStyle {
		return this.nameStyle;
	}

	public hasShader(name: string): boolean {
		return name in this.shaders;
	}

	public getShader(name: string): ShaderFunction {
		if (this.hasShader(name)) {
			return this.shaders[name];
		} else {
			throw new Error(`Could not locate shader ${name}`);
		}
	}
}