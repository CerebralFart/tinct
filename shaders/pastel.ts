import ShaderPack from "./class";
import HSV from "../colors/hsv";

const map = (value, li, hi, lo, ho) => (value - li) / (hi - li) * (ho - lo) + lo;

const pastelShaderPack = new ShaderPack({
	name: 'pastel',
	shaders: {
		pastel: (color, tinct) => {
			let hsv = HSV.fromRGB(color);

			if (hsv.saturation < 5 && hsv.value > 95) return null;

			hsv.saturation = map(hsv.saturation, 0, 100, 30, 50);
			hsv.value = map(hsv.value, 0, 100, 80, 100);

			return hsv.toRGB();
		}
	}
});

export default pastelShaderPack;