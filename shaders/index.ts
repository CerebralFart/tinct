import basicShaderPack from "./basic";
import pastelShaderPack from "./pastel";
import ShaderPack from "./class";

export default ShaderPack;

export {
	basicShaderPack,
	pastelShaderPack,
};