import ShaderPack from "./class";
import Color from "../src/color";
import {mixColors} from "../src/util";

const mixAmount = 0.3;
const white=new Color("#FFFFFF");
const black=new Color("#000000");

const basicShaderPack = new ShaderPack({
	name: 'basic',
	shaders: {
		extralight: (color, tinct) => mixColors(
			color,
			tinct.white || white,
			2 * mixAmount
		),
		light: (color, tinct) => mixColors(
			color,
			tinct.white || white,
			mixAmount
		),
		dark: (color, tinct) => mixColors(
			color,
			tinct.black || black,
			mixAmount
		),
		extradark: (color, tinct) => mixColors(
			color,
			tinct.black || black,
			2 * mixAmount
		),
	}
});

export default basicShaderPack;