import test from 'ava';
import {chunkString, mixColors, parseToComponent} from "../src/util";
import Color from "../src/color";

test('chunk-string', t => {
	const string = "Tinct is awesome";
	const results = {
		1: ['T', 'i', 'n', 'c', 't', ' ', 'i', 's', ' ', 'a', 'w', 'e', 's', 'o', 'm', 'e'],
		2: ['Ti', 'nc', 't ', 'is', ' a', 'we', 'so', 'me'],
		3: ['Tin', 'ct ', 'is ', 'awe', 'som', 'e'],
		4: ['Tinc', 't is', ' awe', 'some'],
		5: ['Tinct', ' is a', 'wesom', 'e'],
		6: ['Tinct ', 'is awe', 'some'],
		7: ['Tinct i', 's aweso', 'me'],
		8: ['Tinct is', ' awesome'],
		9: ['Tinct is ', 'awesome'],
		10: ['Tinct is a', 'wesome'],
		11: ['Tinct is aw', 'esome'],
		12: ['Tinct is awe', 'some'],
		13: ['Tinct is awes', 'ome'],
		14: ['Tinct is aweso', 'me'],
		15: ['Tinct is awesom', 'e'],
		16: ['Tinct is awesome']
	};

	for (const len in results) {
		t.deepEqual(results[len], chunkString(string, len));
	}
});

const parseToComponentHelper = (data, t, msg) => {
	for (let component in data) {
		let expected = data[component];
		let actual = parseToComponent(component);
		t.is(
			actual, expected,
			`Expected ${component} to be parsed to ${expected}, but result was ${actual}`
		);
	}
};

test('parse-component-single', t => {
	let singleChars = {
		'0': 0, '1': 17, '2': 34, '3': 51,
		'4': 68, '5': 85, '6': 102, '7': 119,
		'8': 136, '9': 153, 'a': 170, 'b': 187,
		'c': 204, 'd': 221, 'e': 238, 'f': 255,
	};

	parseToComponentHelper(singleChars, t);
});

test('parse-to-component-multiple', t => {
	let data = {
		'00': 0, '11': 17, '22': 34, '33': 51,
		'44': 68, '55': 85, '66': 102, '77': 119,
		'88': 136, '99': 153, 'aa': 170, 'bb': 187,
		'cc': 204, 'dd': 221, 'ee': 238, 'ff': 255,
		'10': 16, '28': 40, '4f': 79, '51': 81
	};

	parseToComponentHelper(data, t);
});

test('parse-to-component-incorrect', t => {
	let data = {
		'': NaN, 'g': NaN, 'fg': NaN, 'gf': NaN,
		'z': NaN, 'i': NaN, 'q': NaN, 'm': NaN
	};

	parseToComponentHelper(data, t);
});

//TODO test parseColor

test('mix-colors-pure', t => {
	let data = [
		['#808080FF', '#000000', '#FFFFFF', 0.5],
		['#FFFFFFFF', '#000000', '#FFFFFF', 1],
		['#000000FF', '#000000', '#FFFFFF', 0],
		['#FF8080FF', '#FF0000', '#FFFFFF', 0.5],
		['#80FF80FF', '#00FF00', '#FFFFFF', 0.5],
		['#8080FFFF', '#0000FF', '#FFFFFF', 0.5],
		['#800000FF', '#FF0000', '#000000', 0.5],
		['#008000FF', '#00FF00', '#000000', 0.5],
		['#000080FF', '#0000FF', '#000000', 0.5],
		['#808000FF', '#FF0000', '#00FF00', 0.5],
		['#008080FF', '#00FF00', '#0000FF', 0.5],
		['#800080FF', '#0000FF', '#FF0000', 0.5],
	];

	for (let [result, v1, v2, amount] of data) {
		let c1 = new Color(v1);
		let c2 = new Color(v2);

		t.is(
			mixColors(c1, c2, amount).toLowerCase(),
			result.toLowerCase(),
			`Mixing ${v1} and ${v2} should result in ${result} (ratio: ${amount})`
		);
	}
});