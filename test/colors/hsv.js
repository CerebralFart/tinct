import test from 'ava';
import HSV from "../../colors/hsv";
import Color from "../../src/color";

test('rgb-to-hsv', t => {
	let hsvWhite = HSV.fromRGB(new Color('#FFFFFF'));
	t.is(hsvWhite.saturation, 0);
	t.is(hsvWhite.value, 100);

	let hsvBlack = HSV.fromRGB(new Color('#000000'));
	t.is(hsvBlack.value, 0);

	let hsvRed = HSV.fromRGB(new Color('#FF0000'));
	t.is(Math.round(hsvRed.hue), 0);
	t.is(Math.round(hsvRed.saturation), 100);
	t.is(Math.round(hsvRed.value), 100);

	let hsvGreen = HSV.fromRGB(new Color('#00FF00'));
	t.is(Math.round(hsvGreen.hue), 120);
	t.is(Math.round(hsvGreen.saturation), 100);
	t.is(Math.round(hsvGreen.value), 100);

	let hsvBlue = HSV.fromRGB(new Color('#0000FF'));
	t.is(Math.round(hsvBlue.hue), 240);
	t.is(Math.round(hsvBlue.saturation), 100);
	t.is(Math.round(hsvBlue.value), 100);
});

test('hsv-to-rgb', t => {
	t.deepEqual(
		new Color([255, 0, 0]),
		new HSV(0, 100, 100).toRGB()
	);
	t.deepEqual(
		new Color([255, 255, 0]),
		new HSV(60, 100, 100).toRGB()
	);
	t.deepEqual(
		new Color([0, 255, 0]),
		new HSV(120, 100, 100).toRGB()
	);
	t.deepEqual(
		new Color([0, 255, 255]),
		new HSV(180, 100, 100).toRGB()
	);
	t.deepEqual(
		new Color([0, 0, 255]),
		new HSV(240, 100, 100).toRGB()
	);
	t.deepEqual(
		new Color([255, 0, 255]),
		new HSV(300, 100, 100).toRGB()
	);
	t.deepEqual(
		new Color([0, 0, 0]),
		new HSV(0, 100, 0).toRGB()
	);
	t.deepEqual(
		new Color([127, 64, 64]),
		new HSV(0, 50, 50).toRGB()
	)
});