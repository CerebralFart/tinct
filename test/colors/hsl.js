import test from 'ava';
import HSL from "../../colors/hsl";
import Color from "../../src/color";

test('rgb-to-hsl', t => {
	let hslWhite = HSL.fromRGB(new Color('#FFFFFF'));
	t.is(hslWhite.lightness, 1);

	let hslBlack = HSL.fromRGB(new Color('#000000'));
	t.is(hslBlack.lightness, 0);

	let hslRed = HSL.fromRGB(new Color('#FF0000'));
	t.is(hslRed.hue, 0);
	t.is(hslRed.saturation, 1);
	t.is(hslRed.lightness, 0.5);

	let hslGreen = HSL.fromRGB(new Color('#00FF00'));
	t.is(hslGreen.hue, 1 / 3);
	t.is(hslGreen.saturation, 1);
	t.is(hslGreen.lightness, 0.5);

	let hslBlue = HSL.fromRGB(new Color('#0000FF'));
	t.is(hslBlue.hue, 2 / 3);
	t.is(hslBlue.saturation, 1);
	t.is(hslBlue.lightness, 0.5);
});
test('hsl-to-rgb', t => {
	t.is(
		'#000000ff',
		new HSL(0, 0, 0).toRGB().toLowerCase()
	);
	t.is(
		'#ffffffff',
		new HSL(0, 0, 1).toRGB().toLowerCase()
	);
	t.is(
		'#ff0000ff',
		new HSL(0, 1, 0.5).toRGB().toLowerCase()
	);
	t.is(
		'#00ff00ff',
		new HSL(1 / 3, 1, 0.5).toRGB().toLowerCase()
	);
	t.is(
		'#0000ffff',
		new HSL(2 / 3, 1, 0.5).toRGB().toLowerCase()
	);
});