import test from 'ava';
import TinctManager from "../src/manager";

test('proxy-has', t => {
	let manager = new TinctManager();

	t.true('red' in manager);
	t.true('lightRed' in manager);

	t.true('hasSwatch' in manager);

	t.false('non-existent' in manager);
});

test('proxy-get', t => {
	let manager = new TinctManager();

	t.is('#ff0000ff', manager.red.toLowerCase());
	t.is('#ff4d4dff', manager.lightRed.toLowerCase());

	t.throws(() => manager.amber);
});