import test from 'ava';
import TinctManager from '../src/manager';
import materialSwatch from "../swatches/material";
import pastelShaderPack from "../shaders/pastel";

test('has-color', t => {
	let manager = new TinctManager();

	t.true(manager.hasColor('red'));
	t.false(manager.hasColor('amber'));

	manager.loadSwatch(materialSwatch);

	t.true(manager.hasColor('amber'));
});

test('has-shader-pack', t => {
	let manager = new TinctManager();

	t.true(manager.hasShaderPack('basic'));
	t.false(manager.hasShaderPack('pastel'));

	manager.loadShaderPack(pastelShaderPack);

	t.true(manager.hasShaderPack('pastel'));
});

test('has-swatch', t => {
	let manager = new TinctManager();

	t.true(manager.hasSwatch('basic'));
	t.false(manager.hasSwatch('material'));

	manager.loadSwatch(materialSwatch);

	t.true(manager.hasSwatch('material'));
});

test('name-destructuring', t => {
	let manager = new TinctManager();

	manager.loadSwatch(materialSwatch);
	manager.loadShaderPack(pastelShaderPack);

	let cases = {
		'red': {
			swatch: null,
			color: 'red',
			prefixShade: null,
			suffixShade: null
		},
		'lightRed': {
			swatch: null,
			color: 'red',
			prefixShade: 'light',
			suffixShade: null
		},
		'material.lightRed': {
			swatch: 'material',
			color: 'red',
			prefixShade: 'light',
			suffixShade: null
		},
		'material.pastelRed': {
			swatch: 'material',
			color: 'red',
			prefixShade: 'pastel',
			suffixShade: null
		},
		'material.red': {
			swatch: 'material',
			color: 'red',
			prefixShade: null,
			suffixShade: null
		},
	};

	Object.keys(cases).forEach(color => {
		t.deepEqual(
			manager.destructureName(color),
			cases[color],
			`Failed to destructure ${color}`
		)
	})
});

test('resolve-color', t => {
	let manager = new TinctManager();

	manager.loadSwatch(materialSwatch);

	let cases = {
		'red': '#F44336FF',
		'lightRed': '#F77B72FF',
		'darkRed': '#AB2F26FF',
		'green': '#4CAF50FF',
		'lightGreen': '#8BC34AFF',
		'darkGreen': '#357A38FF',
		'basic.green': '#00FF00FF',
		'basic.lightGreen': '#4DFF4DFF',
		'grey': '#9E9E9EFF',
		'basic.grey': '#808080FF',
		'deepOrange': '#FF5722FF'
	};

	for (let color in cases) {
		t.is(
			manager.resolveColor(color).toLowerCase(),
			cases[color].toLowerCase(),
			`Failed to resolve ${color}`
		)
	}
});