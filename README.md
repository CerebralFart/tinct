# Tinct

## Usage
Tinct comes loaded some default colors, so you can start right away. To get a color, simply retrieve it as follows

```js
const green = Tinct.green;
```

## Custom swatches
Customizing the color scheme is easy:

```js
const swatch = new Swatch({
	name: 'awesomeSwatch',
    colors: {
    	red: new Color("#FF0000"),
        green: new Color("#00FF00"),
        blue: new Color("#0000FF")
    },
    shades: {
    	black: new Color("#000000"),
        white: new Color("#FFFFFF")
    }
});
Tinct.loadSwatch(swatch);
```

Tinct supports loading multiple swatches. In the previous example, you can still request `Tinct.purple` as it comes with the default swatch. If you want to remove the basic swatch, you can call either of the functions below, depending on your use-case.

```js
Tinct.removeSwatch('basic');
Tinct.removeAllSwatches();
```

## Custom shaders
Adding a custom shader is similar to adding a custom swatch.

```js
const shader = new Shader({
	name: 'awesomeShader',
    nameStyle: 'prefix',
    shaders: {
    	awesome: (color, manager) => {
        	// Do whatever you want to
        }
    }
});
Tinct.loadShaderPack(shader);
```

Once you've loaded this shader, you can request `Tinct.awesomeRed`.

Again, we support loading multiple shaders. If you want to remove a shader, you can call either function below.

```js
Tinct.removeShaderPack('basic');
Tinct.removeAllShaderPacks();
```

## Color destructuring
To determine what color is requested, Tinct tries to find a color name in the name you provide. What is left before and after the match are used as shaders. So the string `awesomeRed500` is matched as `red`, with the `awesome` and `500` shaders.
Note that this causes confusion with colors as `extraLightBlue`, as Tinct can interpret this as `lightBlue` with the `extra` shader when a `lightBlue` color is loaded.

