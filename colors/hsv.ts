import {HSV as HSVI, range256} from '../src/d';
import Color from "../src/color";

class HSV implements HSVI {
	public static fromRGB(color: Color): HSV {
		let {red, green, blue} = color;
		let alpha = color.getAlphaFraction() / 255;
		red *= alpha;
		green *= alpha;
		blue *= alpha;

		let hue = null;
		let saturation = 0;
		let value = Math.max(red, green, blue);
		let diff = value - Math.min(red, green, blue);

		if (diff != 0) {
			let differ = c => (value - c) / 6 / diff + 1 / 2;
			saturation = diff / value;
			let red2 = differ(red);
			let green2 = differ(green);
			let blue2 = differ(blue);

			if (red === value) {
				hue = blue2 - green2;
			} else if (green === value) {
				hue = (1 / 3) + red2 - blue2;
			} else if (blue === value) {
				hue = (2 / 3) + green2 - red2;
			}

			if (hue < 0) {
				hue += 1;
			} else if (hue > 1) {
				hue -= 1;
			}

			hue *= 360;
		}

		saturation *= 100;
		value *= 100;

		return new HSV(hue, saturation, value);
	}

	hue: number;
	saturation: number;
	value: number;

	constructor(hue, saturation, value) {
		this.hue = hue;
		this.saturation = saturation;
		this.value = value;
	}

	public toRGB(): Color {
		let v = <range256>Math.round(this.value * 2.55);
		let hp = (this.hue || 0) / 60;
		let i = Math.floor(hp);
		let f = hp - i;
		let p = <range256>Math.round(v * (1 - this.saturation / 100));
		let q = <range256>Math.round(v * (1 - f * this.saturation / 100));
		let t = <range256>Math.round(v * (1 - (1 - f) * this.saturation / 100));

		switch (i % 6) {
			case 0:
				return new Color([v, t, p]);
			case 1:
				return new Color([q, v, p]);
			case 2:
				return new Color([p, v, t]);
			case 3:
				return new Color([p, q, v]);
			case 4:
				return new Color([t, p, v]);
			case 5:
				return new Color([v, p, q]);
		}
	}

}

export default HSV;